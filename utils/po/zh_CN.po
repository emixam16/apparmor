# Chinese (Simplified) translation for apparmor
# Copyright (c) 2019 Rosetta Contributors and Canonical Ltd 2019
# This file is distributed under the same license as the apparmor package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: apparmor\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2014-09-14 19:29+0530\n"
"PO-Revision-Date: 2022-08-25 04:53+0000\n"
"Last-Translator: Yulin Yang <yylteam@hotmail.com>\n"
"Language-Team: Chinese (Simplified) <zh_CN@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2022-08-26 04:33+0000\n"
"X-Generator: Launchpad (build 813bec7783fe35df624c0e68cd38fcaa442fb8d1)\n"

#: ../aa-genprof:56
msgid "Generate profile for the given program"
msgstr "生成给定程序的配置文件"

#: ../aa-genprof:57 ../aa-logprof:25 ../aa-cleanprof:24 ../aa-mergeprof:34
#: ../aa-autodep:25 ../aa-audit:25 ../aa-complain:24 ../aa-enforce:24
#: ../aa-disable:24
msgid "path to profiles"
msgstr "配置文件的路径"

#: ../aa-genprof:58 ../aa-logprof:26
msgid "path to logfile"
msgstr "日志文件的路径"

#: ../aa-genprof:59
msgid "name of program to profile"
msgstr "要配置文件的程序名"

#: ../aa-genprof:69 ../aa-logprof:37
#, python-format
msgid "The logfile %s does not exist. Please check the path"
msgstr "日志文件 %s 不存在。请检查路径"

#: ../aa-genprof:75 ../aa-logprof:43 ../aa-unconfined:36
msgid ""
"It seems AppArmor was not started. Please enable AppArmor and try again."
msgstr "看起来 AppArmor 不在运行，请启用 AppArmor 后再试一次。"

#: ../aa-genprof:80 ../aa-mergeprof:47
#, python-format
msgid "%s is not a directory."
msgstr "%s 不是一个目录。"

#: ../aa-genprof:94
#, python-format
msgid ""
"Can't find %(profiling)s in the system path list. If the name of the "
"application\n"
"is correct, please run 'which %(profiling)s' as a user with correct PATH\n"
"environment set up in order to find the fully-qualified path and\n"
"use the full path as parameter."
msgstr ""
"在系统路径列表中找不到%(profiling)s。如果应用程序的名称\n"
"正确，请以设置了正确路径\n"
"环境的用户身份运行“which %(profiling)s”，以查找完全限定路径并\n"
"使用完整路径作为参数。"

#: ../aa-genprof:96
#, python-format
msgid "%s does not exists, please double-check the path."
msgstr "%s 不存在，请确认路径正确。"

#: ../aa-genprof:124
msgid ""
"\n"
"Before you begin, you may wish to check if a\n"
"profile already exists for the application you\n"
"wish to confine. See the following wiki page for\n"
"more information:"
msgstr ""
"\n"
"在开始之前，您可能希望检查\n"
"您希望限制的应用程序的配置文件\n"
"是否已经存在。有关详细信息，请参考\n"
"以下维基页面："

#: ../aa-genprof:126
msgid ""
"Please start the application to be profiled in\n"
"another window and exercise its functionality now.\n"
"\n"
"Once completed, select the \"Scan\" option below in \n"
"order to scan the system logs for AppArmor events. \n"
"\n"
"For each AppArmor event, you will be given the \n"
"opportunity to choose whether the access should be \n"
"allowed or denied."
msgstr ""
"请启动要在另一个窗口中分析的应用程序，并立即执行其功能。\n"
"\n"
"完成后，选择下面的“扫描”选项，以扫描系统日志中的AppArmor事件。\n"
"\n"
"对于每个 AppArmor 事件，您将有机会选择是应允许还是拒绝访问。"

#: ../aa-genprof:147
msgid "Profiling"
msgstr "分析中"

#: ../aa-genprof:165
msgid ""
"\n"
"Reloaded AppArmor profiles in enforce mode."
msgstr ""
"\n"
"重启 AppArmor 到强制模式"

#: ../aa-genprof:166
msgid ""
"\n"
"Please consider contributing your new profile!\n"
"See the following wiki page for more information:"
msgstr ""
"\n"
"请考虑贡献您的新配置文件！\n"
"参阅以下 wiki 页面获取更多信息："

#: ../aa-genprof:167
#, python-format
msgid "Finished generating profile for %s."
msgstr "已完成为 %s 生成配置文件。"

#: ../aa-logprof:24
msgid "Process log entries to generate profiles"
msgstr "处理日志条目以生成配置文件"

#: ../aa-logprof:27
msgid "mark in the log to start processing after"
msgstr "在日志中进行标记，以便在"

#: ../aa-cleanprof:23
msgid "Cleanup the profiles for the given programs"
msgstr "清理指定程序的配置文件"

#: ../aa-cleanprof:25 ../aa-autodep:26 ../aa-audit:27 ../aa-complain:25
#: ../aa-enforce:25 ../aa-disable:25
msgid "name of program"
msgstr "程序名称"

#: ../aa-cleanprof:26
msgid "Silently overwrite with a clean profile"
msgstr "使用干净的配置文件以静默方式覆盖"

#: ../aa-mergeprof:29
msgid "Perform a 2-way or 3-way merge on the given profiles"
msgstr "对给定的配置文件执行双向或三向合并"

#: ../aa-mergeprof:31
msgid "your profile"
msgstr "您的配置文件"

#: ../aa-mergeprof:32
msgid "base profile"
msgstr "基础配置文件"

#: ../aa-mergeprof:33
msgid "other profile"
msgstr "其他配置文件"

#: ../aa-mergeprof:67 ../apparmor/aa.py:2345
msgid ""
"The following local profiles were changed. Would you like to save them?"
msgstr "以下本地配置文件已更改。你想保存它们吗？"

#: ../aa-mergeprof:148 ../aa-mergeprof:430 ../apparmor/aa.py:1767
msgid "Path"
msgstr "路径"

#: ../aa-mergeprof:149
msgid "Select the appropriate mode"
msgstr "选择合适的模式"

#: ../aa-mergeprof:166
msgid "Unknown selection"
msgstr "未知选择"

#: ../aa-mergeprof:183 ../aa-mergeprof:209
msgid "File includes"
msgstr "包括的文件"

#: ../aa-mergeprof:183 ../aa-mergeprof:209
msgid "Select the ones you wish to add"
msgstr "选择要添加的内容"

#: ../aa-mergeprof:195 ../aa-mergeprof:222
#, python-format
msgid "Adding %s to the file."
msgstr "正在添加%s到文件。"

#: ../aa-mergeprof:199 ../apparmor/aa.py:2258
msgid "unknown"
msgstr "未知"

#: ../aa-mergeprof:224 ../aa-mergeprof:275 ../aa-mergeprof:516
#: ../aa-mergeprof:558 ../aa-mergeprof:675 ../apparmor/aa.py:1620
#: ../apparmor/aa.py:1859 ../apparmor/aa.py:1899 ../apparmor/aa.py:2012
#, python-format
msgid "Deleted %s previous matching profile entries."
msgstr "已删除 %s 个以前的匹配配置文件条目。"

#: ../aa-mergeprof:244 ../aa-mergeprof:429 ../aa-mergeprof:629
#: ../aa-mergeprof:656 ../apparmor/aa.py:992 ../apparmor/aa.py:1252
#: ../apparmor/aa.py:1562 ../apparmor/aa.py:1603 ../apparmor/aa.py:1766
#: ../apparmor/aa.py:1958 ../apparmor/aa.py:1994
msgid "Profile"
msgstr "配置文件"

#: ../aa-mergeprof:245 ../apparmor/aa.py:1563 ../apparmor/aa.py:1604
msgid "Capability"
msgstr "特性"

#: ../aa-mergeprof:246 ../aa-mergeprof:480 ../apparmor/aa.py:1258
#: ../apparmor/aa.py:1564 ../apparmor/aa.py:1605 ../apparmor/aa.py:1817
msgid "Severity"
msgstr "严重性"

#: ../aa-mergeprof:273 ../aa-mergeprof:514 ../apparmor/aa.py:1618
#: ../apparmor/aa.py:1857
#, python-format
msgid "Adding %s to profile."
msgstr "正在添加 %s 到配置文件"

#: ../aa-mergeprof:282 ../apparmor/aa.py:1627
#, python-format
msgid "Adding capability %s to profile."
msgstr "将功能 %s 添加到配置文件。"

#: ../aa-mergeprof:289 ../apparmor/aa.py:1634
#, python-format
msgid "Denying capability %s to profile."
msgstr "拒绝功能 %s 到配置文件。"

#: ../aa-mergeprof:439 ../aa-mergeprof:470 ../apparmor/aa.py:1776
#: ../apparmor/aa.py:1807
msgid "(owner permissions off)"
msgstr "(关闭所有者权限)"

#: ../aa-mergeprof:444 ../apparmor/aa.py:1781
msgid "(force new perms to owner)"
msgstr "（强制对所有者使用新的 perm）"

#: ../aa-mergeprof:447 ../apparmor/aa.py:1784
msgid "(force all rule perms to owner)"
msgstr "(强制将所有规则分配给所有者)"

#: ../aa-mergeprof:459 ../apparmor/aa.py:1796
msgid "Old Mode"
msgstr "旧版模式"

#: ../aa-mergeprof:460 ../apparmor/aa.py:1797
msgid "New Mode"
msgstr "新版模式"

#: ../aa-mergeprof:475 ../apparmor/aa.py:1812
msgid "(force perms to owner)"
msgstr "(强制分配给所有者)"

#: ../aa-mergeprof:478 ../apparmor/aa.py:1815
msgid "Mode"
msgstr "模式"

#: ../aa-mergeprof:556
#, python-format
msgid "Adding %(path)s %(mod)s to profile"
msgstr "添加 %(path)s %(mod)s 到配置文件"

#: ../aa-mergeprof:574 ../apparmor/aa.py:1915
msgid "Enter new path: "
msgstr "输入新目录： "

#: ../aa-mergeprof:630 ../aa-mergeprof:657 ../apparmor/aa.py:1959
#: ../apparmor/aa.py:1995
msgid "Network Family"
msgstr "家庭网络"

#: ../aa-mergeprof:631 ../aa-mergeprof:658 ../apparmor/aa.py:1960
#: ../apparmor/aa.py:1996
msgid "Socket Type"
msgstr "接口类型"

#: ../aa-mergeprof:673 ../apparmor/aa.py:2010
#, python-format
msgid "Adding %s to profile"
msgstr "将 %s 添加到配置文件"

#: ../aa-mergeprof:683 ../apparmor/aa.py:2020
#, python-format
msgid "Adding network access %(family)s %(type)s to profile."
msgstr "添加网络访问 %(family)s %(type)s 到配置文件。"

#: ../aa-mergeprof:689 ../apparmor/aa.py:2026
#, python-format
msgid "Denying network access %(family)s %(type)s to profile"
msgstr "拒绝网络访问 %(family)s %(type)s 到配置文件。"

#: ../aa-autodep:23
msgid "Generate a basic AppArmor profile by guessing requirements"
msgstr "通过猜测需求生成基本的 AppArmor 配置文件"

#: ../aa-autodep:24
msgid "overwrite existing profile"
msgstr "覆盖现存配置"

#: ../aa-audit:24
msgid "Switch the given programs to audit mode"
msgstr "将给定程序切换到审计模式"

#: ../aa-audit:26
msgid "remove audit mode"
msgstr "移除审计模式"

#: ../aa-audit:28
msgid "Show full trace"
msgstr "显示完整跟踪"

#: ../aa-complain:23
msgid "Switch the given program to complain mode"
msgstr "将给定的程序切换到投诉模式"

#: ../aa-enforce:23
msgid "Switch the given program to enforce mode"
msgstr "将给定程序切换到强制模式"

#: ../aa-disable:23
msgid "Disable the profile for the given programs"
msgstr "禁用给定程序的配置文件"

#: ../aa-unconfined:28
msgid "Lists unconfined processes having tcp or udp ports"
msgstr "列出具有 TCP 或 UDP 端口的不受限制的进程"

#: ../aa-unconfined:29
msgid "scan all processes from /proc"
msgstr "从 /proc 中扫描所有进程"

#: ../aa-unconfined:81
#, python-format
msgid "%(pid)s %(program)s (%(commandline)s) not confined"
msgstr "%(pid)s %(program)s (%(commandline)s) 不受限制"

#: ../aa-unconfined:85
#, python-format
msgid "%(pid)s %(program)s%(pname)s not confined"
msgstr "%(pid)s %(program)s%(pname)s 不受限制"

#: ../aa-unconfined:90
#, python-format
msgid "%(pid)s %(program)s (%(commandline)s) confined by '%(attribute)s'"
msgstr "%(pid)s %(program)s (%(commandline)s) 由 '%(attribute)s' 限制"

#: ../aa-unconfined:94
#, python-format
msgid "%(pid)s %(program)s%(pname)s confined by '%(attribute)s'"
msgstr "%(pid)s %(program)s%(pname)s 由 '%(attribute)s' 限制"

#: ../apparmor/aa.py:196
#, python-format
msgid "Followed too many links while resolving %s"
msgstr "解析 %s 时跟踪的链接过多"

#: ../apparmor/aa.py:252 ../apparmor/aa.py:259
#, python-format
msgid "Can't find %s"
msgstr "找不到 %s"

#: ../apparmor/aa.py:264 ../apparmor/aa.py:548
#, python-format
msgid "Setting %s to complain mode."
msgstr "正在将 %s 设置为投诉模式。"

#: ../apparmor/aa.py:271
#, python-format
msgid "Setting %s to enforce mode."
msgstr "正在设置 %s 到强制模式"

#: ../apparmor/aa.py:286
#, python-format
msgid "Unable to find basename for %s."
msgstr "找不到 %s 的基本名称。"

#: ../apparmor/aa.py:301
#, python-format
msgid "Could not create %(link)s symlink to %(filename)s."
msgstr "无法创建符号链接 %(link)s 至 %(filename)s 文件。"

#: ../apparmor/aa.py:314
#, python-format
msgid "Unable to read first line from %s: File Not Found"
msgstr "无法从 %s 读取第一行：找不到文件"

#: ../apparmor/aa.py:328
#, python-format
msgid ""
"Unable to fork: %(program)s\n"
"\t%(error)s"
msgstr ""
"无法 fork：%(program)s\n"
"\t%(error)s"

#: ../apparmor/aa.py:449 ../apparmor/ui.py:303
msgid ""
"Are you sure you want to abandon this set of profile changes and exit?"
msgstr "您确定要放弃这组配置文件更改并退出吗？"

#: ../apparmor/aa.py:451 ../apparmor/ui.py:305
msgid "Abandoning all changes."
msgstr "正在放弃所有更改。"

#: ../apparmor/aa.py:464
msgid "Connecting to repository..."
msgstr "正在连接到存储库..."

#: ../apparmor/aa.py:470
msgid "WARNING: Error fetching profiles from the repository"
msgstr "警告：从存储库获取配置文件时出错"

#: ../apparmor/aa.py:550
#, python-format
msgid "Error activating profiles: %s"
msgstr "激活配置文件时出错： %s"

#: ../apparmor/aa.py:605
#, python-format
msgid "%s contains no profile"
msgstr "%s 不包含配置文件"

#: ../apparmor/aa.py:706
#, python-format
msgid ""
"WARNING: Error synchronizing profiles with the repository:\n"
"%s\n"
msgstr "警告：将配置文件与存储库同步时出错：%s\n"

#: ../apparmor/aa.py:744
#, python-format
msgid ""
"WARNING: Error synchronizing profiles with the repository\n"
"%s"
msgstr "警告：将配置文件与存储库 %s 同步时出错"

#: ../apparmor/aa.py:832 ../apparmor/aa.py:883
#, python-format
msgid ""
"WARNING: An error occurred while uploading the profile %(profile)s\n"
"%(ret)s"
msgstr ""
"警告：在上传配置文件 %(profile)s 时发生错误\n"
"%(ret)s"

#: ../apparmor/aa.py:833
msgid "Uploaded changes to repository."
msgstr "已将更改上传到存储库"

#: ../apparmor/aa.py:865
msgid "Changelog Entry: "
msgstr "更新日志条目： "

#: ../apparmor/aa.py:885
msgid ""
"Repository Error\n"
"Registration or Signin was unsuccessful. User login\n"
"information is required to upload profiles to the repository.\n"
"These changes could not be sent."
msgstr ""
"存储库错误\n"
"注册或登录失败。需要用户登录信息才能将配置文件上载到存储库。\n"
"无法传送这些更改。"

#: ../apparmor/aa.py:995
msgid "Default Hat"
msgstr "默认帽子"

#: ../apparmor/aa.py:997
msgid "Requested Hat"
msgstr "已请求帽子"

#: ../apparmor/aa.py:1218
#, python-format
msgid "%s has transition name but not transition mode"
msgstr "%s 具有转换名称，但没有转换模式"

#: ../apparmor/aa.py:1232
#, python-format
msgid "Target profile exists: %s\n"
msgstr "目标配置文件已存在：%s\n"

#: ../apparmor/aa.py:1254
msgid "Program"
msgstr "程序"

#: ../apparmor/aa.py:1257
msgid "Execute"
msgstr "运行"

#: ../apparmor/aa.py:1287
msgid "Are you specifying a transition to a local profile?"
msgstr "是否指定了到本地配置文件的转换？"

#: ../apparmor/aa.py:1299
msgid "Enter profile name to transition to: "
msgstr "输入要转换为的配置文件名称： "

#: ../apparmor/aa.py:1308
msgid ""
"Should AppArmor sanitise the environment when\n"
"switching profiles?\n"
"\n"
"Sanitising environment is more secure,\n"
"but some applications depend on the presence\n"
"of LD_PRELOAD or LD_LIBRARY_PATH."
msgstr ""
"AppArmor 在切换配置文件时是否应该对环境进行清理？\n"
"\n"
"清理环境更安全，但有些应用依赖于是否存在\n"
"LD_PRELOAD 或 LD_LIBRARY_PATH。"

#: ../apparmor/aa.py:1310
msgid ""
"Should AppArmor sanitise the environment when\n"
"switching profiles?\n"
"\n"
"Sanitising environment is more secure,\n"
"but this application appears to be using LD_PRELOAD\n"
"or LD_LIBRARY_PATH and sanitising the environment\n"
"could cause functionality problems."
msgstr ""
"AppArmor 在切换配置文件时是否应该对环境进行清理？\n"
"\n"
"清理环境更安全，但此应用程序似乎正在使用 LD_PRELOAD\n"
" 或 LD_LIBRARY_PATH 并且清理环境可能会导致功能问题。"

#: ../apparmor/aa.py:1318
#, python-format
msgid ""
"Launching processes in an unconfined state is a very\n"
"dangerous operation and can cause serious security holes.\n"
"\n"
"Are you absolutely certain you wish to remove all\n"
"AppArmor protection when executing %s ?"
msgstr ""
"在不受限制的状态下启动进程是一种非常\n"
"危险的操作，可能会导致严重的安全漏洞。\n"
"\n"
"您是否绝对确定在执行 %s 时要删除\n"
"所有 AppArmor 保护？"

#: ../apparmor/aa.py:1320
msgid ""
"Should AppArmor sanitise the environment when\n"
"running this program unconfined?\n"
"\n"
"Not sanitising the environment when unconfining\n"
"a program opens up significant security holes\n"
"and should be avoided if at all possible."
msgstr ""
"AppArmor是否应该在无限制地\n"
"运行此程序时对环境进行清理？\n"
"\n"
"在取消约束程序时不对环境进行\n"
"清理会造成重大的安全漏洞，\n"
"如果可能的话，应避免使用。"

#: ../apparmor/aa.py:1396 ../apparmor/aa.py:1414
#, python-format
msgid ""
"A profile for %s does not exist.\n"
"Do you want to create one?"
msgstr ""
"针对 %s 的配置文件不存在。\n"
"您想创建一个吗？"

#: ../apparmor/aa.py:1523
msgid "Complain-mode changes:"
msgstr "投诉模式更改："

#: ../apparmor/aa.py:1525
msgid "Enforce-mode changes:"
msgstr "强制模式更改："

#: ../apparmor/aa.py:1528
#, python-format
msgid "Invalid mode found: %s"
msgstr "发现无效模式：%s"

#: ../apparmor/aa.py:1897
#, python-format
msgid "Adding %(path)s %(mode)s to profile"
msgstr "正在添加 %(path)s %(mode)s 到配置文件"

#: ../apparmor/aa.py:1918
#, python-format
msgid ""
"The specified path does not match this log entry:\n"
"\n"
"  Log Entry: %(path)s\n"
"  Entered Path:  %(ans)s\n"
"Do you really want to use this path?"
msgstr ""
"指定的路径与此日志条目不匹配：\n"
"\n"
"  日志条目：%(path)s\n"
"  输入的路径：%(ans)s\n"
"是否确实要使用此路径？"

#: ../apparmor/aa.py:2251
#, python-format
msgid "Reading log entries from %s."
msgstr "从 %s 读取日志条目。"

#: ../apparmor/aa.py:2254
#, python-format
msgid "Updating AppArmor profiles in %s."
msgstr "正在在 %s 中更新 AppArmor 配置文件。"

#: ../apparmor/aa.py:2323
msgid ""
"Select which profile changes you would like to save to the\n"
"local profile set."
msgstr "选择你想保存到本地配置文件集的哪些配置文件变化。"

#: ../apparmor/aa.py:2324
msgid "Local profile changes"
msgstr "本地配置文件更改"

#: ../apparmor/aa.py:2418
msgid "Profile Changes"
msgstr "配置文件更改"

#: ../apparmor/aa.py:2428
#, python-format
msgid "Can't find existing profile %s to compare changes."
msgstr "找不到现有的配置文件 %s，无法比较变化。"

#: ../apparmor/aa.py:2566 ../apparmor/aa.py:2581
#, python-format
msgid "Can't read AppArmor profiles in %s"
msgstr "无法读取 %s 中的 AppArmor 配置文件"

#: ../apparmor/aa.py:2677
#, python-format
msgid ""
"%(profile)s profile in %(file)s contains syntax errors in line: %(line)s."
msgstr "%(file)s 中的 %(profile)s 配置文件在以下行中包含语法错误：%(line)s。"

#: ../apparmor/aa.py:2734
#, python-format
msgid ""
"Syntax Error: Unexpected End of Profile reached in file: %(file)s line: "
"%(line)s"
msgstr "语法错误：在文件 %(file)s 第 %(line)s 行中的配置文件意外结束"

#: ../apparmor/aa.py:2749
#, python-format
msgid ""
"Syntax Error: Unexpected capability entry found in file: %(file)s line: "
"%(line)s"
msgstr "语法错误：在文件 %(file)s 第 %(line)s 行中发现意外的功能条目"

#: ../apparmor/aa.py:2770
#, python-format
msgid ""
"Syntax Error: Unexpected link entry found in file: %(file)s line: %(line)s"
msgstr "语法错误：在文件 %(file)s 第 %(line)s 行中发现意外的链接条目"

#: ../apparmor/aa.py:2798
#, python-format
msgid ""
"Syntax Error: Unexpected change profile entry found in file: %(file)s line: "
"%(line)s"
msgstr "语法错误：在文件 %(file)s 第 %(line)s 行中发现意外的更改配置文件条目"

#: ../apparmor/aa.py:2820
#, python-format
msgid ""
"Syntax Error: Unexpected rlimit entry found in file: %(file)s line: %(line)s"
msgstr ""

#: ../apparmor/aa.py:2831
#, python-format
msgid ""
"Syntax Error: Unexpected boolean definition found in file: %(file)s line: "
"%(line)s"
msgstr "语法错误：发现无效的布尔标识符在文件：%(file)s 行：%(line)s"

#: ../apparmor/aa.py:2871
#, python-format
msgid ""
"Syntax Error: Unexpected bare file rule found in file: %(file)s line: "
"%(line)s"
msgstr ""

#: ../apparmor/aa.py:2894
#, python-format
msgid ""
"Syntax Error: Unexpected path entry found in file: %(file)s line: %(line)s"
msgstr ""

#: ../apparmor/aa.py:2922
#, python-format
msgid "Syntax Error: Invalid Regex %(path)s in file: %(file)s line: %(line)s"
msgstr "语法错误：无效的正则表达式 %(path)s 在文件：%(file)s 行：%(line)s"

#: ../apparmor/aa.py:2925
#, python-format
msgid "Invalid mode %(mode)s in file: %(file)s line: %(line)s"
msgstr ""

#: ../apparmor/aa.py:2977
#, python-format
msgid ""
"Syntax Error: Unexpected network entry found in file: %(file)s line: %(line)s"
msgstr ""

#: ../apparmor/aa.py:3007
#, python-format
msgid ""
"Syntax Error: Unexpected dbus entry found in file: %(file)s line: %(line)s"
msgstr ""

#: ../apparmor/aa.py:3030
#, python-format
msgid ""
"Syntax Error: Unexpected mount entry found in file: %(file)s line: %(line)s"
msgstr ""

#: ../apparmor/aa.py:3052
#, python-format
msgid ""
"Syntax Error: Unexpected signal entry found in file: %(file)s line: %(line)s"
msgstr ""

#: ../apparmor/aa.py:3074
#, python-format
msgid ""
"Syntax Error: Unexpected ptrace entry found in file: %(file)s line: %(line)s"
msgstr ""

#: ../apparmor/aa.py:3096
#, python-format
msgid ""
"Syntax Error: Unexpected pivot_root entry found in file: %(file)s line: "
"%(line)s"
msgstr ""

#: ../apparmor/aa.py:3118
#, python-format
msgid ""
"Syntax Error: Unexpected unix entry found in file: %(file)s line: %(line)s"
msgstr ""

#: ../apparmor/aa.py:3140
#, python-format
msgid ""
"Syntax Error: Unexpected change hat declaration found in file: %(file)s "
"line: %(line)s"
msgstr ""

#: ../apparmor/aa.py:3152
#, python-format
msgid ""
"Syntax Error: Unexpected hat definition found in file: %(file)s line: "
"%(line)s"
msgstr ""

#: ../apparmor/aa.py:3168
#, python-format
msgid "Error: Multiple definitions for hat %(hat)s in profile %(profile)s."
msgstr ""

#: ../apparmor/aa.py:3185
#, python-format
msgid "Warning: invalid \"REPOSITORY:\" line in %s, ignoring."
msgstr ""

#: ../apparmor/aa.py:3198
#, python-format
msgid "Syntax Error: Unknown line found in file: %(file)s line: %(line)s"
msgstr ""

#: ../apparmor/aa.py:3211
#, python-format
msgid ""
"Syntax Error: Missing '}' or ','. Reached end of file %(file)s while inside "
"profile %(profile)s"
msgstr ""

#: ../apparmor/aa.py:3277
#, python-format
msgid "Redefining existing variable %(variable)s: %(value)s in %(file)s"
msgstr ""

#: ../apparmor/aa.py:3282
#, python-format
msgid ""
"Values added to a non-existing variable %(variable)s: %(value)s in %(file)s"
msgstr ""

#: ../apparmor/aa.py:3284
#, python-format
msgid ""
"Unknown variable operation %(operation)s for variable %(variable)s in "
"%(file)s"
msgstr ""

#: ../apparmor/aa.py:3343
#, python-format
msgid "Invalid allow string: %(allow)s"
msgstr ""

#: ../apparmor/aa.py:3778
msgid "Can't find existing profile to modify"
msgstr ""

#: ../apparmor/aa.py:4347
#, python-format
msgid "Writing updated profile for %s."
msgstr ""

#: ../apparmor/aa.py:4481
#, python-format
msgid "File Not Found: %s"
msgstr "文件未找到：%s"

#: ../apparmor/aa.py:4591
#, python-format
msgid ""
"%s is currently marked as a program that should not have its own\n"
"profile.  Usually, programs are marked this way if creating a profile for \n"
"them is likely to break the rest of the system.  If you know what you're\n"
"doing and are certain you want to create a profile for this program, edit\n"
"the corresponding entry in the [qualifiers] section in "
"/etc/apparmor/logprof.conf."
msgstr ""

#: ../apparmor/logparser.py:127 ../apparmor/logparser.py:132
#, python-format
msgid "Log contains unknown mode %s"
msgstr "日志包含未知模式 %s"

#: ../apparmor/tools.py:84 ../apparmor/tools.py:126
#, python-format
msgid ""
"Can't find %(program)s in the system path list. If the name of the "
"application\n"
"is correct, please run 'which %(program)s' as a user with correct PATH\n"
"environment set up in order to find the fully-qualified path and\n"
"use the full path as parameter."
msgstr ""

#: ../apparmor/tools.py:86 ../apparmor/tools.py:102 ../apparmor/tools.py:128
#, python-format
msgid "%s does not exist, please double-check the path."
msgstr ""

#: ../apparmor/tools.py:100
msgid ""
"The given program cannot be found, please try with the fully qualified path "
"name of the program: "
msgstr ""

#: ../apparmor/tools.py:113 ../apparmor/tools.py:137 ../apparmor/tools.py:157
#: ../apparmor/tools.py:175 ../apparmor/tools.py:193
#, python-format
msgid "Profile for %s not found, skipping"
msgstr ""

#: ../apparmor/tools.py:140
#, python-format
msgid "Disabling %s."
msgstr ""

#: ../apparmor/tools.py:198
#, python-format
msgid "Setting %s to audit mode."
msgstr ""

#: ../apparmor/tools.py:200
#, python-format
msgid "Removing audit mode from %s."
msgstr ""

#: ../apparmor/tools.py:212
#, python-format
msgid ""
"Please pass an application to generate a profile for, not a profile itself - "
"skipping %s."
msgstr ""

#: ../apparmor/tools.py:220
#, python-format
msgid "Profile for %s already exists - skipping."
msgstr ""

#: ../apparmor/tools.py:232
#, python-format
msgid ""
"\n"
"Deleted %s rules."
msgstr ""

#: ../apparmor/tools.py:240
#, python-format
msgid ""
"The local profile for %(program)s in file %(file)s was changed. Would you "
"like to save it?"
msgstr ""

#: ../apparmor/tools.py:260
#, python-format
msgid "The profile for %s does not exists. Nothing to clean."
msgstr "%s 的配置文件不存在。没什么可清理的。"

#: ../apparmor/ui.py:61
msgid "Invalid hotkey for"
msgstr "无效热键"

#: ../apparmor/ui.py:77 ../apparmor/ui.py:121 ../apparmor/ui.py:275
msgid "(Y)es"
msgstr "是(Y)"

#: ../apparmor/ui.py:78 ../apparmor/ui.py:122 ../apparmor/ui.py:276
msgid "(N)o"
msgstr "否(N)"

#: ../apparmor/ui.py:123
msgid "(C)ancel"
msgstr "取消(C)"

#: ../apparmor/ui.py:223
msgid "(A)llow"
msgstr "允许(A)"

#: ../apparmor/ui.py:224
msgid "(M)ore"
msgstr "更多(M)"

#: ../apparmor/ui.py:225
msgid "Audi(t)"
msgstr "审计(T)"

#: ../apparmor/ui.py:226
msgid "Audi(t) off"
msgstr "关闭审计(T)"

#: ../apparmor/ui.py:227
msgid "Audit (A)ll"
msgstr "审计全部(A)"

#: ../apparmor/ui.py:229
msgid "(O)wner permissions on"
msgstr "打开所有者权限(O)"

#: ../apparmor/ui.py:230
msgid "(O)wner permissions off"
msgstr "关闭所有者权限(O)"

#: ../apparmor/ui.py:231
msgid "(D)eny"
msgstr "拒绝(D)"

#: ../apparmor/ui.py:232
msgid "Abo(r)t"
msgstr "中止(R)"

#: ../apparmor/ui.py:233
msgid "(F)inish"
msgstr "完成(F)"

#: ../apparmor/ui.py:234
msgid "(I)nherit"
msgstr "继承(I)"

#: ../apparmor/ui.py:235
msgid "(P)rofile"
msgstr "配置文件(P)"

#: ../apparmor/ui.py:236
msgid "(P)rofile Clean Exec"
msgstr "配置文件清理执行(P)"

#: ../apparmor/ui.py:237
msgid "(C)hild"
msgstr "儿童模式(C)"

#: ../apparmor/ui.py:238
msgid "(C)hild Clean Exec"
msgstr "子项清理执行(C)"

#: ../apparmor/ui.py:239
msgid "(N)amed"
msgstr "命名(N)"

#: ../apparmor/ui.py:240
msgid "(N)amed Clean Exec"
msgstr "命名清理执行(N)"

#: ../apparmor/ui.py:241
msgid "(U)nconfined"
msgstr ""

#: ../apparmor/ui.py:242
msgid "(U)nconfined Clean Exec"
msgstr ""

#: ../apparmor/ui.py:243
msgid "(P)rofile Inherit"
msgstr "配置文件继承(P)"

#: ../apparmor/ui.py:244
msgid "(P)rofile Inherit Clean Exec"
msgstr "配置文件继承清理执行(P)"

#: ../apparmor/ui.py:245
msgid "(C)hild Inherit"
msgstr ""

#: ../apparmor/ui.py:246
msgid "(C)hild Inherit Clean Exec"
msgstr ""

#: ../apparmor/ui.py:247
msgid "(N)amed Inherit"
msgstr "命名继承(N)"

#: ../apparmor/ui.py:248
msgid "(N)amed Inherit Clean Exec"
msgstr "命名继承清理执行(N)"

#: ../apparmor/ui.py:249
msgid "(X) ix On"
msgstr ""

#: ../apparmor/ui.py:250
msgid "(X) ix Off"
msgstr ""

#: ../apparmor/ui.py:251 ../apparmor/ui.py:265
msgid "(S)ave Changes"
msgstr "保存更改(S)"

#: ../apparmor/ui.py:252
msgid "(C)ontinue Profiling"
msgstr "继续分析(C)"

#: ../apparmor/ui.py:253
msgid "(N)ew"
msgstr "新建(N)"

#: ../apparmor/ui.py:254
msgid "(G)lob"
msgstr ""

#: ../apparmor/ui.py:255
msgid "Glob with (E)xtension"
msgstr ""

#: ../apparmor/ui.py:256
msgid "(A)dd Requested Hat"
msgstr "添加请求的帽子(A)"

#: ../apparmor/ui.py:257
msgid "(U)se Default Hat"
msgstr "使用默认帽子(U)"

#: ../apparmor/ui.py:258
msgid "(S)can system log for AppArmor events"
msgstr "扫描系统日志以查找 AppArmor 事件(S)"

#: ../apparmor/ui.py:259
msgid "(H)elp"
msgstr "帮助(H)"

#: ../apparmor/ui.py:260
msgid "(V)iew Profile"
msgstr "查看配置文件(V)"

#: ../apparmor/ui.py:261
msgid "(U)se Profile"
msgstr "使用配置文件(U)"

#: ../apparmor/ui.py:262
msgid "(C)reate New Profile"
msgstr "新建配置文件(C)"

#: ../apparmor/ui.py:263
msgid "(U)pdate Profile"
msgstr "更新配置文件(U)"

#: ../apparmor/ui.py:264
msgid "(I)gnore Update"
msgstr "忽略更新(I)"

#: ../apparmor/ui.py:266
msgid "Save Selec(t)ed Profile"
msgstr "保存选择的配置文件(T)"

#: ../apparmor/ui.py:267
msgid "(U)pload Changes"
msgstr "上传更改(U)"

#: ../apparmor/ui.py:268
msgid "(V)iew Changes"
msgstr "查看更改(V)"

#: ../apparmor/ui.py:269
msgid "View Changes b/w (C)lean profiles"
msgstr "查看更改黑白并清理配置文件(C)"

#: ../apparmor/ui.py:270
msgid "(V)iew"
msgstr "查看(V)"

#: ../apparmor/ui.py:271
msgid "(E)nable Repository"
msgstr "启用存储库(E)"

#: ../apparmor/ui.py:272
msgid "(D)isable Repository"
msgstr "禁用存储库(D)"

#: ../apparmor/ui.py:273
msgid "(N)ever Ask Again"
msgstr "不再提醒(N)"

#: ../apparmor/ui.py:274
msgid "Ask Me (L)ater"
msgstr "稍后提醒(L)"

#: ../apparmor/ui.py:277
msgid "Allow All (N)etwork"
msgstr "允许所有网络(N)"

#: ../apparmor/ui.py:278
msgid "Allow Network Fa(m)ily"
msgstr "允许网络家族(M)"

#: ../apparmor/ui.py:279
msgid "(O)verwrite Profile"
msgstr "覆盖配置文件(O)"

#: ../apparmor/ui.py:280
msgid "(K)eep Profile"
msgstr "保留配置文件(K)"

#: ../apparmor/ui.py:281
msgid "(C)ontinue"
msgstr "继续(C)"

#: ../apparmor/ui.py:282
msgid "(I)gnore"
msgstr "忽略(I)"

#: ../apparmor/ui.py:344
#, python-format
msgid "PromptUser: Unknown command %s"
msgstr "PromptUser: 未知命令 %s"

#: ../apparmor/ui.py:351
#, python-format
msgid "PromptUser: Duplicate hotkey for %(command)s: %(menutext)s "
msgstr "PromptUser: %(command)s: %(menutext)s 的重复热键 "

#: ../apparmor/ui.py:363
msgid "PromptUser: Invalid hotkey in default item"
msgstr "PromptUser: 默认项中的热键无效"

#: ../apparmor/ui.py:368
#, python-format
msgid "PromptUser: Invalid default %s"
msgstr "PromptUser: 无效的默认值 %s"
